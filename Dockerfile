FROM node 
WORKDIR /blockchain 
COPY package.json . 
RUN yarn install 
COPY . . 
EXPOSE 6009 
CMD yarn start