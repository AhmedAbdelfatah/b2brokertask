import 'dotenv/config';
import WebSocket from 'ws';
import EventType from './constants/EventType';
import { PORT } from './utils';
const HOST = process.env.HOST || 'ws://localhost';
const client = new WebSocket(`${HOST}:${PORT}`);

client.on('open', () => {
  const subscribeRequest = { type: EventType.Subscribe };
  client.send(JSON.stringify(subscribeRequest));

  const countSubRequest = { type: EventType.CountSubscribers };
  client.send(JSON.stringify(countSubRequest));
});

client.on('message', (message: string) => {
  const response = JSON.parse(message);
  console.log(response);
});
