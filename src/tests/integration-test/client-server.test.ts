import WebSocket from 'ws';
import assert from 'assert';
import EventType from '../../constants/EventType';
import EventStatus from '../../constants/EventStatus';
import ErrorMsg from '../../constants/ErrorMsg';

const HOST = 'ws://localhost:9009';
jest.setTimeout(30000);
describe('WebSocket Server', () => {
  let ws: any;
  beforeEach((done) => {
    ws = new WebSocket(HOST);
    ws.on('open', done);
  });

  afterEach(() => {
    ws.close();
  });

  it('should be able to subscribe', (done) => {
    ws.on('message', (message: any) => {
      const data = JSON.parse(message);
      if (data.type === EventType.Subscribe) {
        assert.equal(data.status, EventStatus.Subscribed);
        done();
      }
    });
    ws.send(JSON.stringify({ type: EventType.Subscribe }));
  });

  it('should be able to unsubscribe', (done) => {
    ws.on('message', (message: any) => {
      const data = JSON.parse(message);
      if (data.type === EventType.Unsubscribe) {
        assert.equal(data.status, EventStatus.Unsubscribed);
        done();
      }
    });
    ws.send(JSON.stringify({ type: EventType.Unsubscribe }));
  });

  it('should be able to count subscribers', (done) => {
    ws.send(JSON.stringify({ type: EventType.Subscribe }));
    ws.on('message', (message: string) => {
      const data = JSON.parse(message);
      if (data.type === EventType.CountSubscribers) {
        assert.equal(data.count, 1);
        ws.send(JSON.stringify({ type: EventType.Unsubscribe }));
        done();
      }
    });
    ws.send(JSON.stringify({ type: EventType.CountSubscribers }));
  });

  it('should be able to send error if (Requested method not implemented) ', (done) => {
    ws.send(JSON.stringify({ type: EventType.Error }));

    ws.on('message', (message: any) => {
      const data = JSON.parse(message);
      if (data.type === EventType.Error) {
        assert.equal(data.type, EventType.Error);
        assert.equal(data.error, ErrorMsg.MethodNotImplemented);
        done();
      }
    });
    ws.send(JSON.stringify({ type: EventType.Error }));
  });

  it('should be able to send error if (Bad formatted payload, non JSON) ', (done) => {
    ws.send('any kind of data');
    ws.on('message', (message: any) => {
      const data = JSON.parse(message);
      if (data.type === EventType.Error) {
        assert.equal(data.type, EventType.Error);
        assert.equal(data.error, ErrorMsg.BadPayload);
        done();
      }
    });
    ws.send('any kind of data');
  });
});
