import 'dotenv/config';
import WebSocket from 'ws';
import ErrorMsg from './constants/ErrorMsg';
import EventStatus from './constants/EventStatus';
import EventType from './constants/EventType';
import { sleep, stringifyMsg, PORT } from './utils';

const wss = new WebSocket.Server({ port: PORT });
let subscribers = 0;

wss.on('connection', (ws) => {
  let subscriber = false;
  ws.on('message', async (message: string) => {
    let data;
    try {
      data = JSON.parse(message);
    } catch (err) {
      ws.send(
        stringifyMsg({
          type: EventType.Error,
          error: ErrorMsg.BadPayload
        })
      );
      return;
    }

    switch (data.type) {
      case EventType.Subscribe:
        subscriber = true;
        await sleep(4000);
        subscribers++;
        ws.send(
          stringifyMsg({
            type: EventType.Subscribe,
            status: EventStatus.Subscribed
          })
        );
        break;
      case EventType.Unsubscribe:
        await sleep(8000);
        if (subscriber) {
          subscribers--;
          subscriber = false;
        }
        ws.send(
          stringifyMsg({
            type: EventType.Unsubscribe,
            status: EventStatus.Unsubscribed
          })
        );
        break;
      case EventType.CountSubscribers:
        console.log(subscribers);
        ws.send(
          stringifyMsg({
            type: EventType.CountSubscribers,
            count: subscribers
          })
        );
        break;
      default:
        ws.send(
          stringifyMsg({
            type: EventType.Error,
            error: ErrorMsg.MethodNotImplemented
          })
        );
        break;
    }
  });

  setInterval(() => {
    ws.send(stringifyMsg({ type: EventType.Heatbeat }));
  }, 1000);
});
console.log(`WebSocket server running on port ${PORT}`);
