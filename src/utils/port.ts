let PORT = 8080;
if (typeof process.env.PORT === 'string') {
  PORT = parseFloat(process.env.PORT);
}
if (process.env.NODE_ENV === 'dev') {
  PORT = 9009;
}
export default PORT;
