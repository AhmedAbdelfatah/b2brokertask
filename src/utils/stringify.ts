const stringifyMsg = (data: any) => {
  return JSON.stringify({ ...data, updatedAt: new Date() });
};
export default stringifyMsg;
