enum EventStatus {
  Unsubscribed = 'Unsubscribed',
  Subscribed = 'Subscribed'
}
export default EventStatus;
