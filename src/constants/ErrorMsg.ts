enum ErrorMsg {
  MethodNotImplemented = 'Requested method not implemented',
  BadPayload = 'Bad formatted payload, non JSON'
}
export default ErrorMsg;
