enum EventType {
  Subscribe = 'Subscribe',
  Unsubscribe = 'Unsubscribe',
  CountSubscribers = 'CountSubscribers',
  Error = 'Error',
  Heatbeat = 'Heatbeat'
}
export default EventType;
