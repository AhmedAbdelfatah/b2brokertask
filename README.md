<!--
This is a b2broker task which it's a WS Server/Client produce some events with a callback function holds a data called after event is fired and if you are connected and listening you will receive the data.
-->

<!-- Table of Contents -->

# :notebook_with_decorative_cover: Table of Contents

- [About the Project](#star2-about-the-project)
  - [Screenshots](#camera-screenshots)
  - [Tech Stack](#space_invader-tech-stack)
  - [Features](#dart-features)
  - [Environment Variables](#key-environment-variables)
- [Getting Started](#toolbox-getting-started)
  - [Prerequisites](#bangbang-prerequisites)
  - [Installation](#gear-installation)
  - [Running Tests](#test_tube-running-tests)
  - [Run Locally](#running-run-locally)

<!-- About the Project -->

## :star2: About the Project

Create NodeJS application (Server and Client) that would utilize WebSocket transport and use json as a contract based approach. Server should handle 3 methods: Subscribe, Unsubscribe, CountSubscribers. Common message pattern should looks like this:

```json
{ "type": Subscribe | Unscubscribe | CountSubscribers }
```

In case of Subscribe was requested server should answer with status message Subscribed and timestamp when it took place after await for 4 seconds.

```json
 { "type": Subscribe, "status": "Subscribed", "updatedAt": *** }
```

In case of Unsubscribe was requested server should answer with status message Unsubscribed and timestamp when it took place after await for 8 seconds.

```json
{ "type": Unscubscribe, "status": "Unsubscribed", "updatedAt": *** }
```

Both methods should act with idempotence (should acknowledge current state and return same response for first and all next calls). In case of CountSubscribers was requested server should answer with number of current subscriptions and timestamp when it was counted.

```json
{ "type": CountSubscribers, "count": *** "updatedAt": *** }
```

On any other requests, server should return error message:

```json
 { "type": Error, "error": "Requested method not implemented", "updatedAt": *** }
```

And in case of request was made with non-than json payload server should return this error message:

```json
{ "type": Error, "error": "Bad formatted payload, non JSON", "updatedAt": *** }
```

In addition to this methods, server should produce heartbeat events every second:

```json
 { "type": Heatbeat, "updatedAt": *** }
```

Client part should be written in the same NodeJS application, different module. Both: a server and a client should be tested via integration tests.

<!-- Screenshots -->

### :camera: Screenshots

<div align="center"> 
 <img src="/assets/01.png" alt="Screen-Shot-2023-01-12-at-6-46-01-AM" border="0">
</div>
<div align="center"> 
<img src="/assets/02.png" alt="Screen-Shot-2023-01-12-at-6-46-15-AM" border="0">
</div>
<div align="center"> 
<img src="/assets/03.png" alt="Screen-Shot-2023-01-12-at-6-46-26-AM" border="0">
</div>

<!-- TechStack -->

### :space_invader: Tech Stack

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://www.typescriptlang.org/">Typescript</a></li>
    <li><a href="https://nodejs.dev/en/">NodeJS</a></li>
    <li><a href="https://jestjs.io">Jest</a></li>
    
  </ul>
</details>

<details>
<summary>DevOps</summary>
  <ul>
    <li><a href="https://www.docker.com/">Docker</a></li>
    <li><a href="https://gitlab.com">CICD</a></li>
  </ul>
</details>

<!-- Features -->

### :dart: Features

- Subscribe
- CountSubscribers
- Unsubscribe
- Heatbeat
- Error

<!-- Env Variables -->

### :key: Environment Variables

To run this project, you will need to add the following environment variables to your `.env` file and `.env.example` already pushed to gitlab as a reference

`PORT`

`HOST`

<!-- Getting Started -->

## :eyes: Getting Started

<!-- Prerequisites -->

### :bangbang: Prerequisites

<!-- Installation -->

### :gear: Installation

Install b2brokerTask with npm

```bash
  cd b2brokerTask
  yarn install
```

<!-- Running Tests -->

### :triangular_flag_on_post: Running Tests

To run tests, run the following command start the devlopment server first to run the integration test.

```bash
  yarn run start:dev
  yarn test
```

<!-- Run Locally -->

### :running: Run Locally

Clone the project

```bash
  git clone https://gitlab.com/AhmedAbdelfatah/b2brokertask.git
```

Go to the project directory

```bash
  cd b2brokertask
```

Install dependencies

```bash
  yarn install
```

Start the dev server

```bash
  yarn run start:dev
  yarn run test
```

<div align="center"> <img src="/assets/04.png" alt="Screen-Shot-2023-01-12-at-7-02-34-AM" border="0"></div>

Start the build server

```bash
  yarn run start:server
  yarn run start:client
```
